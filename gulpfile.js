// INCLUDE GULP
var gulp = require('gulp');

// INCLUDE PLUGINS
var livereload = require('gulp-livereload')
var uglify = require('gulp-uglifyjs');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');



// COMPILE OUR SASS FILES
gulp.task('sass', function () {
  gulp.src('./wp-content/themes/template/sass/**/*.scss')
    .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 7', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./wp-content/themes/template/css'));
});

// CONCATENATE & MINIFY JS
gulp.task('uglify', function() {
  gulp.src('./wp-content/themes/template/lib/*.js')
    .pipe(uglify('main.js'))
    .pipe(gulp.dest('./wp-content/themes/template/js'))
});

// WATCH FILES FOR CHANGES
gulp.task('watch', function(){
    livereload.listen();
    gulp.watch('./wp-content/themes/template//sass/**/*.scss', ['sass']);
    gulp.watch('./wp-content/themes/template//lib/*.js', ['uglify']);
    gulp.watch([
        './wp-content/themes/template/**/*.css', 
        './wp-content/themes/template/**/*.php',
        './wp-content/themes/template/**/*.scss',
        './wp-content/themes/template/**/*.js'
        ], function (files){
            livereload.changed(files)
    });
});
