<?php get_header(); ?>
	<section id="content" role="main">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>		
			<div class="page-wrap">
				<div class="page-inner">
					<?php the_content(); ?>
				</div>
			</div>
		<?php endwhile; endif; ?>
	</section>
<?php get_footer(); ?>