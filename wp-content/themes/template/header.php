<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>
		<div id="wrapper" class="hfeed">
			<header id="header" role="banner">
				<section id="logo">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home">
						<img src="<?php echo get_template_directory_uri(); ?>/images/logo.svg" class="logo">
					</a>
				</section>
				<div id="mobile-menu-trigger-wrap" class="collapsed"><div id="mobile-menu-trigger">
					<span class="top"></span>
					<span class="middle"></span>
					<span class="bottom"></span>
				</div></div>
				<nav id="menu" role="navigation">
					<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
				</nav>
				<?php if ( is_active_sidebar( 'header-widget-area' ) ) : ?>
					<section id="header-widget">
						<?php dynamic_sidebar( 'header-widget-area' ); ?>
					</section>
				<?php endif; ?>
				
				
			</header>
			
			<!-- BEGIN CONTENT -->
			<div id="container">