<?php
/*
	Theme: Template
	Author: EdJaleel
	File: functions.php
*/


/*Setup*/
add_action( 'after_setup_theme', 'template_setup' );
function template_setup(){
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	global $content_width;

	if ( ! isset( $content_width ) ) $content_width = 640;
	
	register_nav_menus(
		array( 'main-menu' => __( 'Main Menu', 'template' ) )
	);
}

/*Load Scripts*/
add_action( 'wp_enqueue_scripts', 'template_load_scripts' );
function template_load_scripts(){
	wp_enqueue_script( 'jquery' );
	wp_enqueue_style('template',  get_template_directory_uri() . '/css/style.css');
	wp_enqueue_script('template-js',  get_template_directory_uri() . '/js/main.js');
}

/*Widget Positions*/
add_action( 'widgets_init', 'template_widgets_init' );
function template_widgets_init(){
	register_sidebar( 
		array (
			'name' => __( 'Header Widget Area', 'template' ),
			'id' => 'header-widget-area',
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => '',
		)
	);
}

