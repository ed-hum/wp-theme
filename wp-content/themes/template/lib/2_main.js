jQuery(document).ready(function(c) {
	var $owl;
	var $grid;
    if (c('#gridwrap').length){
        $grid = c('#gridwrap').masonry({
          // options
          itemSelector: '.mblock',
          percentPosition: true,
          gutter: 0,
  		  columnWidth: '.grid-sizer'
        });
    }
    var l = c("body").find(".mbcarousel").length;
    
	c("body").find(".mbcarousel").each(function(){
	    	c(this).find("img").clone().appendTo("#mbcarousel");
	});
	var carNav = "<ul class='carnav'>";
	var x = 0;
	while (x < l){
		carNav += "<li data-index='" + x + "'></li>";
		x++;
	}
	carNav += "</ul>";
	c("#mbcarousel").after(carNav);

    setTimeout(initializeOwl, 1000);
    
    function initializeOwl(){

    	$owl = c(".owl-carousel")
    	

    	$owl.on('changed.owl.carousel',function(e){
    		var i = e.relatedTarget.current();
    		var h = $owl.find('.owl-item:eq(' + i + ')').height();
    		c("#mbcarousel").height(h);
    		c(".carnav").find('li.active').removeClass('active');
    		c(".carnav").find('li:eq(' + i + ')').addClass('active');
			$grid.masonry();    		
    	});

    	$owl.on('initialized.owl.carousel',function(e){
    		var i = e.relatedTarget.current();
    		var h = $owl.find('.owl-item:eq(' + i + ')').height();
    		c("#mbcarousel").height(h);
    		c(".carnav").find('li.active').removeClass('active');
    		c(".carnav").find('li:eq(' + i + ')').addClass('active');
    		$grid.masonry();
    	});

    	$owl.owlCarousel({
	    	items:1,
	    	autoHeight:true,
	    	dots:true,
	    	});
    	$grid.masonry();
    }
    
    c("#mobile-menu-trigger").click(function(){
    	c(this).parents("#mobile-menu-trigger-wrap").toggleClass('collapsed');
    	c("nav#menu").toggleClass('active');
    });

});
