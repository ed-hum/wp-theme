<?php get_header(); ?>
<section id="content" role="main">
<article id="post-0" class="post not-found">
<header class="header">
<h1 class="entry-title">404 - Page Not Found</h1>
</header>
<section class="entry-content">
<p>Nothing found for the requested page. Try a search instead?</p>
</section>
</article>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>