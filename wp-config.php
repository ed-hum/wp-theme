<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wptemplate');

/** MySQL database username */
define('DB_USER', 'wptemplate');

/** MySQL database password */
define('DB_PASSWORD', 'wptemplate');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ch>>>Lb2B#c9N{TquD+50Z;%xX/{#Kfaq.AV>c$.sI=eQ}S>h!3]H}+$93qt{udl');
define('SECURE_AUTH_KEY',  '9CTNd[*#FB(E$d-YtBGkD{bKXLAs(()rmL%mReUh]cLO-xS=>]AK1Y8wyJ5^<8o3');
define('LOGGED_IN_KEY',    'ik1&xeM:E%A1Nwn`]Z~+NVa3E7,}>FJ}YGb Vp6hZD6iREOU94NuGjU5D54|l>Qq');
define('NONCE_KEY',        '>Q?#%i+][@{H(r<fW|}rEsJR%%KfO;w/xT>dE&gjnTv++v~U5|!tU,^Wmt$Jqq*{');
define('AUTH_SALT',        '*O!Q)n%)4&db&>1Zk;s!_~{eQ2{LOG3_RU#WZ.qq.HYHgy#ZMa8CE.lo6X&,)p}}');
define('SECURE_AUTH_SALT', '21eaQ} mAC5`a I0,+cuGeV)A3<uTh0x;^|yVMh}iR:m7`D6$Tm[/JD3nrK:!Y%#');
define('LOGGED_IN_SALT',   'jg>Cz&@HVT/7Mkc4(<AZ:eQUas#s!k`[@}al?#jV,kebCZ!g%}<P~ Qlsj0vd4en');
define('NONCE_SALT',       '#v/om+{lt$7]$:Gau!I6[3I@1Gn7{h0Ua=,3l+8[&(w5*mphB{HNOt qYLC8$X).');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
